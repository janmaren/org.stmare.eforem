package org.stmare.eforem;

import static org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchDelegate;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.internal.core.LaunchConfiguration;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.core.CompilationUnit;
import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.m2e.actions.MavenLaunchConstants;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.stmare.util.AttributeUtil;
import org.stmare.util.M2EUtil;

@SuppressWarnings("restriction")
public class M2TestShortcut implements org.eclipse.debug.ui.ILaunchShortcut {

    private static final String DEBUG_MAVEN_DELEGATE_ID = "org.stmare.eforem.debugMavenDelegate";
    public static final String VM_SET_DEBUG = "maven.surefire.debug=true";
    private static final String VM_TEST_PAR = "test";

    @Override
    public void launch(ISelection selection, String mode) {
        if (selection instanceof TreeSelection) {
            TreeSelection treeSelection = (TreeSelection) selection;
            if (treeSelection.getFirstElement() instanceof CompilationUnit) {
                CompilationUnit compilationUnit = (CompilationUnit) treeSelection.getFirstElement();
                IType primaryType = compilationUnit.getCompilationUnit().findPrimaryType();
                if (primaryType != null) {
                    String canonMTestName = primaryType.getFullyQualifiedName();
                    try {
                        IProject project = compilationUnit.getCompilationUnit().getCorrespondingResource().getProject();
                        doLaunch(canonMTestName, mode, project);
                    } catch (JavaModelException e) {
                        Activator.logError(e);
                    }
                }
            }
        }
    }

    @Override
    public void launch(IEditorPart editorPart, String mode) {
        if (editorPart instanceof CompilationUnitEditor) {
            SelectionInfoManager.SelectionInfo selectionInfo = SelectionInfoManager.getSelectionInfo(editorPart);
            if (selectionInfo != null) {
                IFile file = (IFile) editorPart.getEditorInput().getAdapter(IFile.class);
                String canonMTestName = selectionInfo.getCanonMTestName();
                doLaunch(canonMTestName, mode, file.getProject());
            }
        }
    }
    
    private void doLaunch(String canonMTestName, String mode, IContainer basedir) {
        ILaunchConfiguration previousConfig = getPreviousConfig(canonMTestName);
        if (previousConfig != null) {
            DebugUITools.launch(previousConfig, mode);
            return;
        }
        
        String configName = getConfigName(canonMTestName, mode);
        ILaunchConfiguration configByName = getConfigByName(configName);
        if (configByName != null) {
            try {
                configByName.delete(); // preparing for next creation under such name
            } catch (CoreException e) {
                Activator.logError(e);
            } 
        }

        ILaunchConfigurationWorkingCopy workingCopy = M2EUtil.createLaunchConfiguration(basedir, "test", configName);
        addTestConfiguration(workingCopy, canonMTestName, mode);
        addDebugDelegate(workingCopy);

        try {
            ILaunchConfiguration launchConfiguration = workingCopy.doSave();
            int result = DebugUITools.openLaunchConfigurationPropertiesDialog(
                    PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
                    launchConfiguration,
                    DebugUITools.getLaunchGroup(launchConfiguration, mode).getIdentifier());
            if (result != Dialog.OK) {
                getPreviousConfig(canonMTestName).delete();
            } else {
                DebugUITools.launch(launchConfiguration, mode);
            }
        } catch (CoreException e) {
            Activator.logError(e);
            return;
        }
    }

    public void addDebugDelegate(ILaunchConfigurationWorkingCopy workingCopy) {
        Set<String> modes = new HashSet<String>();
        modes.add(ILaunchManager.DEBUG_MODE);
        workingCopy.setPreferredLaunchDelegate(modes, DEBUG_MAVEN_DELEGATE_ID);
    }

    private ILaunchConfiguration getPreviousConfig(String canonMTestName) {
        ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
        ILaunchConfigurationType type = manager.getLaunchConfigurationType(MavenLaunchConstants.LAUNCH_CONFIGURATION_TYPE_ID);
        ILaunchConfiguration[] configurations;
        try {
            configurations = manager.getLaunchConfigurations(type);
            for (int i = 0; i < configurations.length; i++) {
                ILaunchConfiguration configuration = configurations[i];
                String vmArguments = (String) configuration.getAttributes().get(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS);
                if (AttributeUtil.vmArgumentsContains(vmArguments, "test=" + canonMTestName)) {
                    return configuration;
                }
            }
        } catch (CoreException e1) {
            Activator.logError(e1);
        }
        return null;
    }

    private ILaunchConfiguration getConfigByName(String configName) {
        ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
        ILaunchConfigurationType type = manager.getLaunchConfigurationType(MavenLaunchConstants.LAUNCH_CONFIGURATION_TYPE_ID);
        ILaunchConfiguration[] configurations;
        try {
            configurations = manager.getLaunchConfigurations(type);
            for (int i = 0; i < configurations.length; i++) {
                ILaunchConfiguration configuration = configurations[i];
                if (configName.equals(configuration.getName())) {
                    return configuration;
                }
            }
        } catch (CoreException e1) {
            Activator.logError(e1);
        }
        return null;
    }
    
    private String getConfigName(String canonMTestName, String mode) {
        String name = canonMTestName;
        int lastIndexOf = canonMTestName.lastIndexOf(".");
        if (lastIndexOf != -1) {
            name = name.substring(lastIndexOf + 1); 
        }
        name = name.replace("#", ".");
        return "Test " + name;
    }
    
    private void addTestConfiguration(ILaunchConfigurationWorkingCopy workingCopy, String canonMTestName, String mode) {
        StringBuilder sb = new StringBuilder();
        sb.append("-D").append(VM_TEST_PAR).append("=").append(canonMTestName);
        
        if (ILaunchManager.DEBUG_MODE.equals(mode)) {
            sb.append(" ").append("-D").append(VM_SET_DEBUG);
        }
        
        workingCopy.setAttribute(ATTR_VM_ARGUMENTS, sb.toString());
    }    
}
