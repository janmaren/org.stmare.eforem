package org.stmare.eforem.delegate;

import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.internal.ui.views.console.ProcessConsole;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.m2e.actions.MavenLaunchConstants;
import org.eclipse.m2e.internal.launch.MavenLaunchDelegate;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.stmare.eforem.Activator;
import org.stmare.eforem.M2TestShortcut;
import org.stmare.util.AttributeUtil;
import org.stmare.util.ProjectUtil;
import org.stmare.util.RemoteJavaUtil;

@SuppressWarnings("restriction")
public class DebugMavenDelegate extends MavenLaunchDelegate {
    private static final String LOCALHOST = "localhost";

    private static final String MAVEN_DEBUG_PORT = "5005";
    
    private static final String WAITING_DEBUG_MESSAGE = "Listening for transport dt_socket at address: 5005";
    
    @Override
    public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor) throws CoreException {
        String vmAttribute = configuration.getAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS, "");
        if (AttributeUtil.vmArgumentsContainsTest(vmAttribute) && !AttributeUtil.vmArgumentsContains(vmAttribute, M2TestShortcut.VM_SET_DEBUG)) {
            ILaunchConfigurationWorkingCopy workingCopy = configuration.getWorkingCopy();
            @SuppressWarnings("unchecked")
            Map<String, ?> attributes = workingCopy.getAttributes();
            String vmArguments = (String) attributes.get(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS);
            vmArguments += " -D" + M2TestShortcut.VM_SET_DEBUG;
            workingCopy.setAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS, vmArguments);
            configuration = workingCopy.doSave();
        }
        String projectAbsoluteToOsStringPath = configuration.getAttribute(MavenLaunchConstants.ATTR_POM_DIR, (String) null);
        IProject project = ProjectUtil.getProjectByAbsolutePath(projectAbsoluteToOsStringPath);
        ILaunchConfiguration debugConfiguration = getConfigByPort(LOCALHOST, MAVEN_DEBUG_PORT, project != null ? project.getName(): null);
        if (debugConfiguration == null) {
            ILaunchConfigurationWorkingCopy launchConfiguration = RemoteJavaUtil.createLaunchConfiguration("Debug 5005", project != null ? project.getName(): null);
            try {
                debugConfiguration = launchConfiguration.doSave();
            } catch (CoreException e) {
                Activator.logError(e);
            }
        }
        
        new Thread(new LaunchDebug(debugConfiguration, configuration.getName())).start();
        
        super.launch(configuration, mode, launch, monitor);
    }

    @SuppressWarnings("unchecked")
    private final static ILaunchConfiguration getConfigByPort(String host, String port, String projectToOsStringPath) {
        ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
        ILaunchConfigurationType type = manager.getLaunchConfigurationType("org.eclipse.jdt.launching.remoteJavaApplication");
        ILaunchConfiguration[] configurations;
        try {
            configurations = manager.getLaunchConfigurations(type);
            for (int i = 0; i < configurations.length; i++) {
                ILaunchConfiguration configuration = configurations[i];
                if (!configuration.exists()) {
                    continue;
                }
                if (projectToOsStringPath != null) {
                    if (!projectToOsStringPath.equals(configuration.getAttribute("org.eclipse.jdt.launching.PROJECT_ATTR", (String) null))) {
                        continue;
                    }
                }
                Map<?, ?> attributes = configuration.getAttributes();
                Map<String, Map<String, String>> connectMap = (Map<String, Map<String, String>>) attributes.get("org.eclipse.jdt.launching.CONNECT_MAP");
                if (connectMap != null) {
                    if (host.equals(connectMap.get("hostname")) && port.equals(connectMap.get("port"))) {
                        return configuration;
                    }
                }
            }
        } catch (CoreException e1) {
            Activator.logError(e1);
        }
        return null;
    }    
    
    private class LaunchDebug implements Runnable {
        private final static long MAX_TRY_PERIOD = 10000;

        private final static long SLEEP_PERIOD = 250;

        private final static long MAX_ATTEMPTS = 6;

        private ILaunchConfiguration debugConfiguration;

        private String runName;

        public LaunchDebug(ILaunchConfiguration debugConfiguration, String runName) {
            this.debugConfiguration = debugConfiguration;
            this.runName = runName;
        }

        @Override
        public void run() {
            if (debugConfiguration == null) {
                return;
            }

            int i = 0;
            do {
                long tryUntil = System.currentTimeMillis() + MAX_TRY_PERIOD;
                while (System.currentTimeMillis() < tryUntil) {
                    try {
                        Thread.sleep(SLEEP_PERIOD);
                    } catch (InterruptedException e) {
                        return;
                    }
                    
                    try {
                        debugConfiguration.launch(ILaunchManager.DEBUG_MODE, null);
                        return;
                    } catch (CoreException e) {
                        // no handling
                    }
                }
                ProcessConsole processConsole = findProcessConsole(runName);
                if (processConsole != null) {
                    String string = processConsole.getDocument().get();
                    if (string.contains(WAITING_DEBUG_MESSAGE)) {
                        try {
                            debugConfiguration.launch(ILaunchManager.DEBUG_MODE, null);
                            return;
                        } catch (CoreException e) {
                            Activator.logError(e, "Maven waits for port 5005 but launched debug configuration returned exception");
                        }
                        return;
                    }
                }
            } while (i++ < MAX_ATTEMPTS);
        }
    }
    
    public static ProcessConsole findProcessConsole(String runName) {
        ConsolePlugin plugin = ConsolePlugin.getDefault();
        IConsoleManager conMan = plugin.getConsoleManager();
        IConsole[] existing = conMan.getConsoles();
        for (int i = 0; i < existing.length; i++) {
            IConsole iConsole = existing[i];
            if ("org.eclipse.debug.ui.ProcessConsoleType".equals(iConsole.getType()) && iConsole.getName().startsWith(runName)) {
                ProcessConsole processConsole = (ProcessConsole) iConsole;
                return processConsole;
            }
        }
        return null;
    } 
}
