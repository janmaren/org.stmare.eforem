package org.stmare.eforem.delegate;

import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.m2e.internal.launch.MavenLaunchDelegate;
import org.stmare.eforem.M2TestShortcut;
import org.stmare.util.AttributeUtil;

@SuppressWarnings("restriction")
public class RunMavenDelegate extends MavenLaunchDelegate {
    
    @Override
    public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor) throws CoreException {
        String vmAttribute = configuration.getAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS, "");
        if (AttributeUtil.vmArgumentsContainsTest(vmAttribute) && AttributeUtil.vmArgumentsContains(vmAttribute, M2TestShortcut.VM_SET_DEBUG)) {
            ILaunchConfigurationWorkingCopy workingCopy = configuration.getWorkingCopy();
            @SuppressWarnings("unchecked")
            Map<String, ?> attributes = workingCopy.getAttributes();
            String vmArguments = (String) attributes.get(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS);
            vmArguments = AttributeUtil.removeDebugFromVmArguments(vmArguments);
            workingCopy.setAttribute(IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS, vmArguments);
            configuration = workingCopy.doSave();
        }

        super.launch(configuration, mode, launch, monitor);
    }
}
