package org.stmare.eforem;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IEditorPart;
import org.stmare.util.AstUtil;

@SuppressWarnings("restriction")
public class SelectionInfoManager {

    public static class SelectionInfo {
        public String className;
        
        /**
         * Selected method name or null when none
         */
        public String methodName;
    
        @Override
        public String toString() {
            return getCanonMTestName();
        }
        
        public String getCanonMTestName() {
            return className + (methodName != null ? "#" + methodName : "");
        }
    }
    
    public static SelectionInfo getSelectionInfo(CompilationUnit compilationUnit, int offset) {
        String className = null;
        String methodName = null;

        ASTNode coveringNode = null;
        if (offset != -1) {
            coveringNode = AstUtil.calculateCoveringASTNode(compilationUnit, offset);
        }
        
        SelectionInfo selectionInfo = new SelectionInfo();
        TypeDeclaration typeDeclaration = null;
        if (coveringNode instanceof SimpleName) {
            SimpleName simpleName = (SimpleName) coveringNode;
            ASTNode parent = coveringNode.getParent();
            if (parent != null && parent.getNodeType() == ASTNode.METHOD_DECLARATION) {
                MethodDeclaration methodDeclaration = (MethodDeclaration) parent;
                if (AstUtil.isJUnitAnnotation(methodDeclaration, compilationUnit)) {
                    methodName = simpleName.getIdentifier();
                    ASTNode classNode = parent.getParent();
                    if (classNode instanceof TypeDeclaration) {
                        typeDeclaration = (TypeDeclaration) classNode;
                        selectionInfo.methodName = methodName;
                        // TODO: check for number of parameters = 0
                    }
                }
            } else if (parent instanceof TypeDeclaration) {
                typeDeclaration = (TypeDeclaration) parent;
            }
        } else if (coveringNode instanceof TypeDeclaration) {
            typeDeclaration = (TypeDeclaration) coveringNode;
        }
        if (typeDeclaration != null) {
            SimpleName nameForClass = typeDeclaration.getName();
            String packageName = AstUtil.getPackageName(compilationUnit);
            if (packageName != null) {
                className = packageName + "." + nameForClass.getFullyQualifiedName();
            } else {
                className = nameForClass.getFullyQualifiedName();
            }
            selectionInfo.className = className;
            return selectionInfo;
        }
        return null;
    }
    
    public static SelectionInfo getSelectionInfo(IEditorPart editorPart) {
        CompilationUnitEditor compUnitEdit = (CompilationUnitEditor) editorPart;
        Point selectedRange = compUnitEdit.getViewer().getSelectedRange();
        CompilationUnit compilationUnit = (CompilationUnit) AstUtil.parseCompilationUnit((ICompilationUnit) compUnitEdit.getViewPartInput());
        return getSelectionInfo(compilationUnit, selectedRange.x);
    }

}
