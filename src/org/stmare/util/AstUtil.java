package org.stmare.util;

import java.util.List;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ImportDeclaration;
import org.eclipse.jdt.core.dom.MarkerAnnotation;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;

public class AstUtil {

    public static boolean existsImport(String packageName, String className, CompilationUnit cu) {
        @SuppressWarnings("unchecked")
        List<ImportDeclaration> imports = cu.imports();
        for (ImportDeclaration importDeclaration : imports) {
            Name name = importDeclaration.getName();
            String fullyQualifiedName = name.getFullyQualifiedName();
            if (fullyQualifiedName.equals(packageName) || fullyQualifiedName.equals(packageName + "." + className)) {
                return true;
            }
        }
        return false;
    }

    public static String getPackageName(CompilationUnit cu) {
        PackageDeclaration packageDeclaration = cu.getPackage();
        if (packageDeclaration == null) {
            return null;
        }
        Name name = packageDeclaration.getName();
        return name.getFullyQualifiedName();
    }
    
    public static boolean isJUnitAnnotation(MethodDeclaration methodDeclaration, CompilationUnit cu) {
        List<?> modifiers = methodDeclaration.modifiers();
        for (Object object : modifiers) {
            if (object instanceof MarkerAnnotation) {
                MarkerAnnotation markerAnnotation = (MarkerAnnotation) object;
                Name typeName = markerAnnotation.getTypeName();
                String fullyQualifiedName = typeName.getFullyQualifiedName();
                if (fullyQualifiedName.equals("org.junit.Test")) {
                    return true;
                }
                if (fullyQualifiedName.equals("Test")) {
                    if (existsImport("org.junit", "Test", cu)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static ASTNode parseCompilationUnit(ICompilationUnit compilationUnit) {
        ASTParser parser = ASTParser.newParser(AST.JLS4);
        parser.setResolveBindings(true);
        parser.setSource(compilationUnit);
        ASTNode ast = parser.createAST(null);
        return ast;
    }

    public static ASTNode calculateCoveringASTNode(ASTNode ast, int position) {
        ASTNode coveringNode = null;
        ASTNode lower = ast;
        while(lower != null) {
            coveringNode = lower;
            lower = null;
            @SuppressWarnings("unchecked")
            List<StructuralPropertyDescriptor> properties = coveringNode.structuralPropertiesForType();
            for(StructuralPropertyDescriptor descriptor : properties) {
                if(descriptor.isChildProperty()) {
                    ASTNode child = (ASTNode) coveringNode.getStructuralProperty(descriptor);
                    if(child != null && AstUtil.covers(child, position)) {
                        lower = child;
                    }
                } else if(descriptor.isChildListProperty()) {
                    @SuppressWarnings("unchecked")
                    List<? extends ASTNode> children = (List<? extends ASTNode>) coveringNode.getStructuralProperty(descriptor);
                    for(ASTNode child : children) {
                        if(AstUtil.covers(child, position)) {
                            lower = child;
                            //calculateCoveringASTNode((ICompilationUnit) lower);
                            break;
                        }
                    }
                }
            }
        }
        return coveringNode;
    }

    public static boolean covers(ASTNode node, int position) {
        return node.getStartPosition() < position && node.getStartPosition() + node.getLength() > position;
    }

}
