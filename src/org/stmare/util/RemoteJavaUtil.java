package org.stmare.util;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.stmare.eforem.Activator;

public class RemoteJavaUtil {
    public static ILaunchConfigurationWorkingCopy createLaunchConfiguration(String name, String projectName) {
        try {
            ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
            ILaunchConfigurationType launchConfigurationType = launchManager.getLaunchConfigurationType("org.eclipse.jdt.launching.remoteJavaApplication");
    
            ILaunchConfigurationWorkingCopy workingCopy = launchConfigurationType.newInstance(null, name);
            Map<String, String> connectMap = new HashMap<String, String>();
            connectMap.put("port", "5005");
            connectMap.put("hostname", "localhost");
            workingCopy.setAttribute("org.eclipse.jdt.launching.CONNECT_MAP", connectMap);
            workingCopy.setAttribute("org.eclipse.debug.ui.private", true);
            if (projectName != null) {
                workingCopy.setAttribute("org.eclipse.jdt.launching.PROJECT_ATTR", projectName);
            }

            return workingCopy;
        } catch (CoreException ex) {
            Activator.logError(ex);
        }
        return null;
    }
}
