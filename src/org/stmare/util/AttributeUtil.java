package org.stmare.util;

public class AttributeUtil {

    public static boolean vmArgumentsContainsTest(String vmArguments) {
        if (vmArguments == null) {
            return false;
        }
        String[] split = vmArguments.split("\\s");
        String expected = "-Dtest=";
        for (String string : split) {
            if (string.startsWith(expected)) {
                return true;
            }
        }
        return false;
    }

    public static boolean vmArgumentsContains(String vmArguments, String expression) {
        if (vmArguments == null) {
            return false;
        }
        String[] split = vmArguments.split("\\s");
        String expected = "-D" + expression;
        for (String string : split) {
            if (string.equals(expected)) {
                return true;
            }
        }
        return false;
    }

    public static String removeDebugFromVmArguments(String vmArguments) {
        if (vmArguments == null) {
            return null;
        }
        String[] split = vmArguments.split("\\s");
        StringBuilder sb = new StringBuilder();
        
        String expected = "-Dmaven.surefire.debug";
        String expected2 = "-Dmaven.surefire.debug=";
        for (String string : split) {
            if (string.startsWith(expected2) || string.equals(expected)) {
                continue;
            }
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(string);
        }
        
        return sb.toString();
    }
    
}
