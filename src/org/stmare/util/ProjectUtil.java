package org.stmare.util;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;

public class ProjectUtil {
    public static IProject getProjectByAbsolutePath(String toOsStringPath) {
        if (toOsStringPath == null ) {
            return null;
        }
        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        IWorkspaceRoot root = workspace.getRoot();
        IProject[] projects = root.getProjects();
        for (IProject iProject : projects) {
            String osString = iProject.getLocation().toOSString();
            if (toOsStringPath.equals(osString)) {
                return iProject;
            }
        }
        return null;
    }
}
