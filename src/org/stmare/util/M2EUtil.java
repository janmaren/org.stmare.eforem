package org.stmare.util;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.RefreshTab;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.m2e.actions.MavenLaunchConstants;
import org.eclipse.m2e.core.MavenPlugin;
import org.eclipse.m2e.core.project.IMavenProjectFacade;
import org.eclipse.m2e.core.project.IMavenProjectRegistry;
import org.eclipse.m2e.core.project.ResolverConfiguration;
import org.stmare.eforem.Activator;

@SuppressWarnings("restriction")
public class M2EUtil {

    public static ILaunchConfigurationWorkingCopy createLaunchConfiguration(IContainer basedir, String goal, String name) {
        try {
            ILaunchManager launchManager = DebugPlugin.getDefault().getLaunchManager();
            ILaunchConfigurationType launchConfigurationType = launchManager.getLaunchConfigurationType("org.eclipse.m2e.Maven2LaunchConfigurationType");
    
            ILaunchConfigurationWorkingCopy workingCopy = launchConfigurationType.newInstance(null, name);
            workingCopy.setAttribute(MavenLaunchConstants.ATTR_POM_DIR, basedir.getLocation().toOSString());
            workingCopy.setAttribute("M2_GOALS", goal);
            workingCopy.setAttribute("org.eclipse.debug.ui.private", false);
            workingCopy.setAttribute(RefreshTab.ATTR_REFRESH_SCOPE, "${project}");
            workingCopy.setAttribute(RefreshTab.ATTR_REFRESH_RECURSIVE, true);
    
            M2EUtil.setProjectConfiguration(workingCopy, basedir);
    
            IPath path = M2EUtil.getJREContainerPath(basedir);
            if (path != null) {
                workingCopy.setAttribute(IJavaLaunchConfigurationConstants.ATTR_JRE_CONTAINER_PATH, path.toPortableString());
            }
    
            return workingCopy;
        } catch (CoreException ex) {
            Activator.logError(ex);
        }
        return null;
    }

    public static void setProjectConfiguration(ILaunchConfigurationWorkingCopy workingCopy, IContainer basedir) {
        IMavenProjectRegistry projectManager = MavenPlugin.getMavenProjectRegistry();
        IFile pomFile = basedir.getFile(new Path("pom.xml"));
        IMavenProjectFacade projectFacade = projectManager.create(pomFile, false, new NullProgressMonitor());
        if (projectFacade != null) {
            ResolverConfiguration configuration = projectFacade.getResolverConfiguration();
    
            String selectedProfiles = configuration.getSelectedProfiles();
            if ((selectedProfiles != null) && (selectedProfiles.length() > 0))
                workingCopy.setAttribute("M2_PROFILES", selectedProfiles);
        }
    }

    public static IPath getJREContainerPath(IContainer basedir) throws CoreException {
        IProject project = basedir.getProject();
        if ((project != null) && (project.hasNature("org.eclipse.jdt.core.javanature"))) {
            IJavaProject javaProject = JavaCore.create(project);
            IClasspathEntry[] entries = javaProject.getRawClasspath();
            for (int i = 0; i < entries.length; i++) {
                IClasspathEntry entry = entries[i];
                if (JavaRuntime.JRE_CONTAINER.equals(entry.getPath().segment(0))) {
                    return entry.getPath();
                }
            }
        }
        return null;
    }

}
